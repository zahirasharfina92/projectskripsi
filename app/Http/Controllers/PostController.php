<?php

namespace App\Http\Controllers;

use App\Models\Post; //import model post
use Illuminate\View\View; //return type view
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(): View // add new method
    {
        $posts = Post::latest()->paginate(5); //get post

        return view('posts.index', compact('posts')); //render view w/ posts
    }
}
